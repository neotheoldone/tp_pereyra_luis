from os import system #Importo la libreria system de OS para utilizar alguna de los metodos como el cls que limpia la pantalla
from msvcrt import getch # importo la libreria para poder usar el getch
system("cls")
Hueco="O" #cada elemento le asigno el caracter que quiero mostrar, luego podria animarlos
Caja="="
Player="5"
class Nivel: #creo la clase nivel 
    def __init__(self,pantalla,hueco,jugador,caja,nroNivel):
        self.Pantalla=pantalla
        self.Hueco=hueco
        self.Jugador=jugador
        self.Caja=caja
        self.NroNivel=nroNivel

nivel1=Nivel([["#","#","#","#","#","#","#","#","#"],["#"," "," "," "," "," ","#"," ","#"],["#"," "," "," ","#"," ", "#"," ","#"],["#"," "," "," ","#"," ","#"," ","#"],["#", " "," "," ","#"," ", " "," " ,"#"],["#", " " ," "," "," "," ", " "," " ,"#"],["#"," " ," "," "," "," ", " " ," " ,"#"],["#", " "," "," "," "," "," " ," " ,"#"],["#","#","#","#","#","#","#","#","#"]],[2,3],[5, 4],[6,6],"1")
nivel2=Nivel([["#","#","#","#","#","#","#","#","#"],["#"," "," "," "," "," "," "," ","#"],["#"," "," "," "," "," ", " "," ","#"],["#"," "," "," ","#"," ","#"," ","#"],["#", " "," "," ","#"," ", " "," " ,"#"],["#", " " ," "," "," "," ", " "," " ,"#"],["#"," " ," "," "," "," ", " " ," " ,"#"],["#", " "," "," "," "," "," " ," " ,"#"],["#","#","#","#","#","#","#","#","#"]],[1,3],[7, 7],[5,3],"2") #instancio una nivel de la clase nivel y le paso todos los paramentros


def update(toma):#funcion que imprime la pantalla actualizada
    fil=9
    col=9    
    system("cls")
    
    for f in range(fil):
        for c in range(col):
            print(toma[f][c],end='  ')
                
        print()
    
def Felicidades(llegaste): #funcion que muestra el mensaje de felicitaciones y maneja los niveles en este caso solo hay dos pero le podria hacer un for
    system("cls")
    print("FELICITACIONES Pasaste el nivel " + llegaste)
    system("pause")
    if llegaste is "1":
        juego(nivel2)
    else:
        print("Llegaste al final del juego muy triste")


def Derrota(llegaste):
    system("cls")
    print("Saliste!! llegaste hasta el nivel:  "+ llegaste)
    
            
                


def juego(nivel): #funcion principal que toma como referencia un objeto de la clase Nivel
    pos = nivel.Jugador
    caja=nivel.Caja
    hueco=nivel.Hueco
    pantalla=nivel.Pantalla  
    esteNivel=nivel.NroNivel 

    pantalla[pos[1]][pos[0]]=Player #reemplazo los caracteres de jugador, caja y hueco pasado anteriormente y lo imprimo en pantalla
    pantalla[caja[1]][caja[0]]=Caja
    pantalla[hueco[1]][hueco[0]]=Hueco
    update(pantalla)
   
    while True:
        pantalla[hueco[1]][hueco[0]]=Hueco #En cada Frame le digo que dibuje el hueco para que no se me pise con el borrado del personaje
        key = ord(getch()) #tomo el key
          
        if key == 27: #Si oprimo Escape rompe el while y sale de la funcion que ahora en si es el juego completo
           Derrota(esteNivel) 
           break     # Pero en un futuro juego mas grande puede terminar el nivel y volver a la posicion que tenia anteriormente  
        
                
        elif key ==80: #Down arrow//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if (fdown(pos,pantalla)and pantalla[pos[1]+1][pos[0]]is not Caja): #primera interpretacion en caso de que no haya colision con caja
                pos[1] += 1 #llame a la funcion fdown y le pase pos que es la pocision relativa del jugador y me devolvera si hay pared cerca
                pantalla[pos[1]][pos[0]]=5
           
                if (pantalla[pos[1]-1][pos[0]]is not "#" ): #comparo que la posicion anterior no sea pared asi no me la borra
                    if (pantalla[pos[1]-1][pos[0]] is not Hueco): #si no es hueco tampoco
                        pantalla[pos[1]-1][pos[0]]=" " #borro el caracter que estaba antes que yo como jugador
                update(pantalla)

            if (fdown(pos,pantalla)and pantalla[pos[1]+1][pos[0]]is Caja and fdown(caja,pantalla)): #Si hay contacto con caja y la caja no esta pegada a la pared lo muevo
                pos[1] += 1
                caja[1]+=1
                if (pantalla[pos[1]][pos[0]]is Caja):
                    pantalla[pos[1]+1][pos[0]]=Caja
                pantalla[pos[1]][pos[0]]=5
                pantalla[pos[1]][pos[0]]=5
           
                if (pantalla[pos[1]-1][pos[0]]is not "#" ):
                    if (pantalla[pos[1]-1][pos[0]] is not Hueco):
                        pantalla[pos[1]-1][pos[0]]=" "
                update(pantalla)
        
            
        elif key == 72: #Up arrow/////////////////////////////////////////////////////////////////////////////////////////
            
            if (fup(pos,pantalla)and pantalla[pos[1]-1][pos[0]]is not Caja):
                pos[1] -= 1
                pantalla[pos[1]][pos[0]]=5
           
                if (pantalla[pos[1]+1][pos[0]]is not "#" ):
                    if (pantalla[pos[1]+1][pos[0]] is not Hueco):
                        pantalla[pos[1]+1][pos[0]]=" "
                update(pantalla)


            if (fup(pos,pantalla)and pantalla[pos[1]-1][pos[0]]is Caja and fup(caja,pantalla)):
                pos[1] -= 1
                caja[1]-=1 
                if (pantalla[pos[1]][pos[0]]is Caja): 
                    pantalla[pos[1]-1][pos[0]]=Caja
                pantalla[pos[1]][pos[0]]=5
           
                if (pantalla[pos[1]+1][pos[0]]is not "#" ):
                    if (pantalla[pos[1]+1][pos[0]] is not Hueco):
                        pantalla[pos[1]+1][pos[0]]=" "
                update(pantalla)

        elif key == 75: #Left arrow //////////////////////////////////////////////////////////////////////////////////////
            if (fleft(pos,pantalla)and pantalla[pos[1]][pos[0]-1]is not Caja): #primera interpretacion en caso de que no haya colision con caja
                pos[0] -= 1
                pantalla[pos[1]][pos[0]]=5
           
                if (pantalla[pos[1]][pos[0]+1]is not "#" ): #comparo que la posicion anterior no sea pared asi no me la borra
                    if (pantalla[pos[1]][pos[0]+1] is not Hueco): #si no es hueco tampoco
                        pantalla[pos[1]][pos[0]+1]=" " #borro el caracter que estaba antes que yo como jugador
                update(pantalla)

            if  fleft(pos,pantalla)and pantalla[pos[1]][pos[0]-1] is Caja and fleft(caja,pantalla): #Si hay contacto con caja y la caja no esta pegada a la pared lo muevo
                pos[0] -= 1
                caja[0]-=1
                pantalla[pos[1]][pos[0]-1]=Caja
                pantalla[pos[1]][pos[0]]=5
                
           
                if (pantalla[pos[1]][pos[0]+1]is not "#" ):
                    if (pantalla[pos[1]][pos[0]+1] is not Hueco):
                        pantalla[pos[1]][pos[0]+1]=" "
                update(pantalla)
            

        elif key == 77: #Right arrow
            if (fright(pos,pantalla)and pantalla[pos[1]][pos[0]+1]is not Caja): #primera interpretacion en caso de que no haya colision con caja
                pos[0] += 1
                pantalla[pos[1]][pos[0]]=5
           
                if (pantalla[pos[1]][pos[0]-1]is not "#" ): #comparo que la posicion anterior no sea pared asi no me la borra
                    if (pantalla[pos[1]][pos[0]-1] is not Hueco): #si no es hueco tampoco
                        pantalla[pos[1]][pos[0]-1]=" " #borro el caracter que estaba antes que yo como jugador
                update(pantalla)

            if fright(pos,pantalla)and pantalla[pos[1]][pos[0]+1] is Caja and fright(caja,pantalla): #Si hay contacto con caja y la caja no esta pegada a la pared lo muevo
                pos[0] += 1
                caja[0]+=1
                pantalla[pos[1]][pos[0]+1]=Caja
                pantalla[pos[1]][pos[0]]=5
                
           
                if (pantalla[pos[1]][pos[0]-1]is not "#" ):
                    if (pantalla[pos[1]][pos[0]-1] is not Hueco):
                        pantalla[pos[1]][pos[0]-1]=" "
                update(pantalla)


        if caja==hueco: #Si finalizado el movimiento se da que la caja callo en el hueco entonces el nivel finalizo
                Felicidades(esteNivel)
                break         
    

#Metodos de movimientos X=0 Y=1
def fright(pos,pantalla):
    if(pantalla[pos[1]][pos[0]+1] is not "#"):#Mientras no llege a algun muro continua el cambio de posicion 
        return True

def fleft(pos,pantalla):
    if(pantalla[pos[1]][pos[0]-1] is not "#"):
        return True


def fup(pos,pantalla):
    if(pantalla[pos[1]-1][pos[0]] is not "#"):
        return True

def fdown(pos,pantalla):
    if(pantalla[pos[1]+1][pos[0]] is not "#"):
        return True #retorno la nueva posicion


juego(nivel1)
