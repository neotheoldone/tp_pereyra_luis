#CALCULADORA PYTHON by LUIS SEBASTIAN PEREYRA V1.00b

from os import system #Importo la libreria system de OS para utilizar alguna de los metodos como el cls que limpia la pantalla


def Suma(total,var1):#defino funciones que toman valores y devuelven la operacion
    return var1+total

def Resta(total,var1):
    return total-var1

def Producto(total,var1):
    return total*var1

def Division(total,var1):
    return total/var1


def Calculadora():  
    
    Menu()
    Total=float(input("Ingrese el primer valor\n")) # Pido el primer valor porque aun no se ha realizado ninguna operacion
    print("PreTotal= ", Total)  #Muestro el pre total ingresado
    opc=int(input("Ingrese Una Operacion\n")) #llamo a la primera operacion antes de entrar en el bucle del while
    while (opc >0 and opc <6): #Solo permito los valores del 1 al 5 fuera de esos valores aviso que no esta permitido y finalizo la calc
        
        if (opc==1):
            valor1= float(input("Ingrese un numero a sumar\n"))
            Total=Suma(Total,valor1)#Llamo a la funcion Division para que me retorne el valor del total dividido el valor ingresado
            Menu()
            print("PreTotal= ", Total)
            opc=int(input("Ingrese Una Operacion\n"))
        
        elif(opc==2):
            valor1= float(input("Ingrese numero a restar\n"))
            Total=Resta(Total,valor1)#Llamo a la funcion Resta para que me retorne el valor del total menos el valor ingresado
            Menu()
            print("PreTotal= ", Total)
            opc=int(input("Ingrese Una Operacion\n"))
        
        elif(opc==3):
            valor1= float(input("Ingrese numero a multiplicar\n"))
            Total=Producto(Total,valor1)#Llamo a la funcion Producto para que me retorne el valor del total Multiplicado el valor ingresado
            Menu()
            print("PreTotal= ", Total)
            opc=int(input("Ingrese Una Operacion\n"))
        
        elif(opc==4):
            try:
                valor1= float(input("Ingrese numero a dividir\n"))
                Total=Division(Total,valor1)#Llamo a la funcion Division para que me retorne el valor del total dividido el valor ingresado
                Menu()
                print("PreTotal= ", Total)
                opc = float(input("Selecione Opcion\n"))
            except ZeroDivisionError:#Agarro el error si el usuario intenta dividir por 0
              print ("No se Permite la Division Entre 0")
              opc=int(input("Ingrese Una Operacion\n"))

        elif(opc==5):
            opcion=input("Realmente quiere salir de la aplicacion s/n\n") #Pregunto si realmente quiere salir de la calculadora
            if (opcion=="s"):
                opc=7
            elif(opc=="n"):
                opc=int(input("Ingrese Una Operacion\n"))
            else:
                print("Opcion no conocida") # si no elige alguna de las dos opciones entiendo que quiere seguir operando y evito otro bucle para que solo conteste si o no
                opc=int(input("Ingrese Una Operacion\n"))
    print("Opcion no conocida Hasta luego!")


def Menu():#Metodo que solo muestra y limpia el menu
    system("cls")#limpio la pantalla con la funcion cls que importo de la libreria OS.System
    """Funcion que Muestra el Menu"""
    print ("""************************************
        Menu Calculadora
************************************
Operaciones disponibles
_______________________
1) Suma
2) Resta
3) Multiplicacion
4) Division
5) Salir""")


Calculadora()#Llamo a la funcion Calculadora recien aca empieza a ejecutar el programa...


